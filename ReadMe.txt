The execution plan of the application is present in run.cmd file. The application uses SQL running in a container for database. You will require Docker and port 1433 available on your system.

Following is what the script will do when executed.

1. Clear the container zipuserssql if it already exists.
2. Start a new container named zipuserssql based on SQL Server 2017 image. Since the container requires some time to come up, the script will pause execution and will require your input. In general, you should wait for 10 seconds before making the execute further by pressing any key on the terminal.
3. Run functional tests by invoking HTTP endpoints of the API.
4. Clear the container.
5. Run unit tests.
6. Recreate the zipuserssql container for the application. Wait for some time before making the script execute further by pressing any key.
7. Run the application. You should be able to browse the application on http://localhost:5000/swagger.
