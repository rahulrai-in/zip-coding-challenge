docker rm -f zipuserssql 
docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=NoTaP@ssW0rd" -p 1433:1433 --name zipuserssql -d mcr.microsoft.com/mssql/server:2017-latest
echo "Press any key when database is ready.."
pause
echo "Running functional tests..."
dotnet test tests\Zip.Users.Tests.FunctionalTests/Zip.Users.Tests.FunctionalTests.csproj
echo "Discarding database"...
docker container rm -f zipuserssql
echo "Running unit tests..."
dotnet test tests\Zip.Users.Tests.UnitTests\Zip.Users.Tests.UnitTests.csproj
echo "Recreating database for application"
docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=NoTaP@ssW0rd" -p 1433:1433 --name zipuserssql -d mcr.microsoft.com/mssql/server:2017-latest
echo "Press any key when database is ready.."
pause
echo "Starting application..."
dotnet run --project src/Zip.Users.Api
pause