﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Zip.Users.Api.Application.Commands;
using Zip.Users.Api.Infrastructure.Repositories;

namespace Zip.Users.Api.Application.CommandHandler
{
    public class CreateAccountCommandHandler : IRequestHandler<CreateAccountCommand, bool>
    {
        private readonly IUserRepository _userRepository;

        public CreateAccountCommandHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<bool> Handle(CreateAccountCommand request, CancellationToken cancellationToken)
        {
            // validate user exists
            var user = await _userRepository.GetUserWithAccountsAsync(request.UserId);
            if (user == null)
            {
                throw new ValidationException("User does not exist.");
            }

            user.AddAccount(request.Name);
            return await _userRepository.SaveChangesAsync();
        }
    }
}