﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using Zip.Users.Api.Application.Commands;
using Zip.Users.Api.Domain.Aggregates;
using Zip.Users.Api.Infrastructure.Repositories;

namespace Zip.Users.Api.Application.CommandHandler
{
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, bool>
    {
        private readonly IUserRepository _userRepository;

        public CreateUserCommandHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<bool> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            // validate whether user exists.
            var user = await _userRepository.GetUserByEmailAsync(request.Email);
            if (user != null)
            {
                throw new ValidationException("User already exists");
            }

            var userFromCommand =
                new ZipUser(request.Name, request.Email, request.MonthlyExpense, request.MonthlySalary);
            _userRepository.Add(userFromCommand);
            return await _userRepository.SaveChangesAsync();
        }
    }
}