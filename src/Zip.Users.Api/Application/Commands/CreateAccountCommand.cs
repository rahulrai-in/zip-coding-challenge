﻿using System.Runtime.Serialization;
using MediatR;

namespace Zip.Users.Api.Application.Commands
{
    public class CreateAccountCommand : IRequest<bool>
    {
        [DataMember] public int UserId { get; set; }

        [DataMember] public string Name { get; set; }
    }
}