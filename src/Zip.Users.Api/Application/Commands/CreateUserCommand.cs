﻿using System.Runtime.Serialization;
using MediatR;

namespace Zip.Users.Api.Application.Commands
{
    public class CreateUserCommand : IRequest<bool>
    {
        [DataMember] public string Name { get; set; }

        [DataMember] public string Email { get; set; }

        [DataMember] public double MonthlySalary { get; set; }

        [DataMember] public double MonthlyExpense { get; set; }
    }
}