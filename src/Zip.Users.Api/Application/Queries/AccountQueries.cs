﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Zip.Users.Api.Domain.Aggregates;
using Zip.Users.Api.Infrastructure.Repositories;

namespace Zip.Users.Api.Application.Queries
{
    public class AccountQueries : IAccountQueries
    {
        private readonly IAccountRepository _accountRepository;

        public AccountQueries(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        public async Task<IList<Account>> GetAccountsAsync()
        {
            return await _accountRepository.GetAccountsAsync();
        }
    }
}