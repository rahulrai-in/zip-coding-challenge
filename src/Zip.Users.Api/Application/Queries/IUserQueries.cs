﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Zip.Users.Api.Domain.Aggregates;

namespace Zip.Users.Api.Application.Queries
{
    public interface IUserQueries
    {
        Task<ZipUser> GetUserAsync(int id);
        Task<IList<ZipUser>> GetUsersAsync();
    }
}