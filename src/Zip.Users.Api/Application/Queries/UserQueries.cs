﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Zip.Users.Api.Domain.Aggregates;
using Zip.Users.Api.Infrastructure.Repositories;

namespace Zip.Users.Api.Application.Queries
{
    public class UserQueries : IUserQueries
    {
        private readonly IUserRepository _userRepository;

        public UserQueries(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<ZipUser> GetUserAsync(int id)
        {
            return await _userRepository.GetUserWithAccountsAsync(id);
        }

        public async Task<IList<ZipUser>> GetUsersAsync()
        {
            return await _userRepository.GetUsersWithAccountsAsync();
        }
    }
}