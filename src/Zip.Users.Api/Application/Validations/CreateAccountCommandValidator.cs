﻿using FluentValidation;
using Zip.Users.Api.Application.Commands;

namespace Zip.Users.Api.Application.Validations
{
    public class CreateAccountCommandValidator : AbstractValidator<CreateAccountCommand>
    {
        public CreateAccountCommandValidator()
        {
            RuleFor(u => u.UserId).GreaterThan(0);
            RuleFor(u => u.Name).NotEmpty().MaximumLength(50);
        }
    }
}