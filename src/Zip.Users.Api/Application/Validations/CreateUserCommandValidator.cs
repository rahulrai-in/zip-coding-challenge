﻿using FluentValidation;
using Zip.Users.Api.Application.Commands;

namespace Zip.Users.Api.Application.Validations
{
    public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserCommandValidator()
        {
            RuleFor(u => u.Name).NotEmpty().MaximumLength(50);
            RuleFor(u => u.Email).NotEmpty().MaximumLength(50);
            RuleFor(u => u.MonthlySalary).GreaterThan(0);
            RuleFor(u => u.MonthlyExpense).GreaterThan(0);
        }
    }
}