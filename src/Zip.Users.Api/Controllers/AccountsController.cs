﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Zip.Users.Api.Application.Commands;
using Zip.Users.Api.Application.Queries;
using Zip.Users.Api.Domain.Aggregates;

namespace Zip.Users.Api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class AccountsController : ControllerBase
    {
        private readonly IAccountQueries _accountQueries;
        private readonly IMediator _mediator;

        public AccountsController(
            IMediator mediator,
            IAccountQueries accountQueries)
        {
            _mediator = mediator;
            _accountQueries = accountQueries;
        }

        [HttpPost]
        [Route("")]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        [ProducesResponseType((int) HttpStatusCode.Created)]
        public async Task<ActionResult> CreateAccountAsync([FromBody] CreateAccountCommand createAccountCommand)
        {
            var commandResult = await _mediator.Send(createAccountCommand);
            if (!commandResult)
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpGet]
        [Route("")]
        [ProducesResponseType(typeof(List<Account>), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<ActionResult> GetAccountsAsync()
        {
            var accounts = await _accountQueries.GetAccountsAsync();
            if (accounts != null)
            {
                return Ok(accounts);
            }

            return NotFound();
        }
    }
}