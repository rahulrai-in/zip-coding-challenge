﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Zip.Users.Api.Application.Commands;
using Zip.Users.Api.Application.Queries;
using Zip.Users.Api.Domain.Aggregates;

namespace Zip.Users.Api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IUserQueries _userQueries;

        public UsersController(
            IMediator mediator,
            IUserQueries userQueries)
        {
            _mediator = mediator;
            _userQueries = userQueries;
        }

        /// <summary>
        ///     Creates new user.
        /// </summary>
        /// <param name="createUserCommand"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        [ProducesResponseType((int) HttpStatusCode.Created)]
        public async Task<ActionResult> CreateUserAsync([FromBody] CreateUserCommand createUserCommand)
        {
            var commandResult = await _mediator.Send(createUserCommand);
            if (!commandResult)
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        ///     Get user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id:int}")]
        [ProducesResponseType(typeof(ZipUser), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<ActionResult> GetUserAsync(int id)
        {
            var user = await _userQueries.GetUserAsync(id);
            if (user != null)
            {
                return Ok(user);
            }

            return NotFound(id);
        }

        /// <summary>
        ///     Get all users
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [ProducesResponseType(typeof(List<ZipUser>), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        public async Task<ActionResult> GetUsersAsync()
        {
            var users = await _userQueries.GetUsersAsync();
            if (users != null)
            {
                return Ok(users);
            }

            return NotFound();
        }
    }
}