namespace Zip.Users.Api.Domain.Aggregates
{
    public class Account
    {
        public Account(string name)
        {
            Name = name;
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}