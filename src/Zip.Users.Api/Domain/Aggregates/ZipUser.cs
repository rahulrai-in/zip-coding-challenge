using System.Collections.Generic;

namespace Zip.Users.Api.Domain.Aggregates
{
    public class ZipUser
    {
        private readonly List<Account> _accounts = new List<Account>();

        public ZipUser(string name, string email, double monthlyExpense, double monthlySalary)
        {
            Name = name;
            Email = email;
            MonthlyExpense = monthlyExpense;
            MonthlySalary = monthlySalary;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public double MonthlySalary { get; set; }
        public double MonthlyExpense { get; set; }
        public IEnumerable<Account> Accounts => _accounts.AsReadOnly();

        public void AddAccount(string accountName)
        {
            _accounts.Add(new Account(accountName));
        }
    }
}