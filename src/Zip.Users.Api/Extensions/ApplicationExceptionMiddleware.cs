﻿using System;
using System.Net;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Zip.Users.Api.Extensions
{
    public class ApplicationExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ApplicationExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (ValidationException vex)
            {
                await HandleExceptionAsync(httpContext, HttpStatusCode.BadRequest, vex.Message);
            }
#pragma warning disable CA1031 // Do not catch general exception types
            catch (Exception ex)
            {
                var message =
                    "Application encountered an error while executing this request. This error might not be transient.";
                await HandleExceptionAsync(httpContext, HttpStatusCode.InternalServerError, message);
            }
#pragma warning restore CA1031 // Do not catch general exception types
        }

        private static async Task HandleExceptionAsync(HttpContext context, HttpStatusCode statusCode, string message)
        {
            context.Response.ContentType = "application/problem+json";
            context.Response.StatusCode = (int) statusCode;
            await context.Response.WriteAsync(JsonConvert.SerializeObject(new
            {
                context.Response.StatusCode,
                Message = message
            }));
        }
    }
}