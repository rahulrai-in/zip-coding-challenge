﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Zip.Users.Api.Domain.Aggregates;

namespace Zip.Users.Api.Infrastructure.EntityConfiguration
{
    public class AccountEntityTypeConfiguration : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.ToTable("account", UsersContext.DefaultSchema);

            builder.HasKey(a => a.Id);
            builder.Property(a => a.Id).UseHiLo("account_hilo", UsersContext.DefaultSchema);
            builder.Property(a => a.Name).HasMaxLength(50).IsRequired();
        }
    }
}