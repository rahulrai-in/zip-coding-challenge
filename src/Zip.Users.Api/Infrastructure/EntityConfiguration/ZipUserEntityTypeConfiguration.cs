﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Zip.Users.Api.Domain.Aggregates;

namespace Zip.Users.Api.Infrastructure.EntityConfiguration
{
    public class ZipUserEntityTypeConfiguration : IEntityTypeConfiguration<ZipUser>
    {
        public void Configure(EntityTypeBuilder<ZipUser> builder)
        {
            builder.ToTable("zipuser", UsersContext.DefaultSchema);

            builder.HasKey(u => u.Id);
            builder.Property(u => u.Id).UseHiLo("zipuser_hilo", UsersContext.DefaultSchema);
            builder.Property(u => u.Email).HasMaxLength(50).IsRequired();
            builder.HasIndex(u => u.Email).IsUnique();
            builder.Property(u => u.MonthlySalary).IsRequired();
            builder.Property(u => u.MonthlyExpense).IsRequired();

            builder.HasMany(u => u.Accounts).WithOne().HasForeignKey("UserId").OnDelete(DeleteBehavior.Cascade);
            builder.Metadata.FindNavigation(nameof(ZipUser.Accounts)).SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}