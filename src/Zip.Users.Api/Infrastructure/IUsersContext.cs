﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Zip.Users.Api.Infrastructure
{
    public interface IUsersContext
    {
        IQueryable<T> GetQuery<T>()
            where T : class;

        EntityEntry<T> Add<T>(T entity)
            where T : class;

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}