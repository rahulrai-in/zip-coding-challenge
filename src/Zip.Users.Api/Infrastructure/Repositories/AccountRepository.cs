﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Zip.Users.Api.Domain.Aggregates;

namespace Zip.Users.Api.Infrastructure.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private readonly IUsersContext _context;

        public AccountRepository(IUsersContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<IList<Account>> GetAccountsAsync()
        {
            return await _context.GetQuery<Account>().ToListAsync();
        }
    }
}