﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Zip.Users.Api.Domain.Aggregates;

namespace Zip.Users.Api.Infrastructure.Repositories
{
    public interface IAccountRepository
    {
        Task<IList<Account>> GetAccountsAsync();
    }
}