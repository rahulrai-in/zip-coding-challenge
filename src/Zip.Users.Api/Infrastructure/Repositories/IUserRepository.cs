﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Zip.Users.Api.Domain.Aggregates;

namespace Zip.Users.Api.Infrastructure.Repositories
{
    public interface IUserRepository
    {
        Task<ZipUser> GetUserByEmailAsync(string email);
        void Add(ZipUser user);
        Task<bool> SaveChangesAsync();
        Task<IList<ZipUser>> GetUsersWithAccountsAsync();
        Task<ZipUser> GetUserWithAccountsAsync(int id);
    }
}