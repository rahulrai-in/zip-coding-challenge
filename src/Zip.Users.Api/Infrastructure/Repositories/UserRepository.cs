﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Zip.Users.Api.Domain.Aggregates;

namespace Zip.Users.Api.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IUsersContext _context;

        public UserRepository(IUsersContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Task<ZipUser> GetUserByEmailAsync(string email)
        {
            return _context.GetQuery<ZipUser>().Where(u => u.Email.Equals(email)).FirstOrDefaultAsync();
        }

        public void Add(ZipUser user)
        {
            _context.Add(user);
        }

        public async Task<bool> SaveChangesAsync()
        {
            var result = await _context.SaveChangesAsync(CancellationToken.None);
            return result > -1;
        }

        public async Task<IList<ZipUser>> GetUsersWithAccountsAsync()
        {
            return await _context.GetQuery<ZipUser>().Include(u => u.Accounts).ToListAsync();
        }

        public async Task<ZipUser> GetUserWithAccountsAsync(int id)
        {
            return await _context.GetQuery<ZipUser>().Include(u => u.Accounts).Where(u => u.Id == id)
                .FirstOrDefaultAsync();
        }
    }
}