﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Zip.Users.Api.Infrastructure.EntityConfiguration;

namespace Zip.Users.Api.Infrastructure
{
    public sealed class UsersContext : DbContext, IUsersContext
    {
        public const string DefaultSchema = "users";

        public UsersContext(DbContextOptions options)
            : base(options)
        {
        }

        public IQueryable<T> GetQuery<T>() where T : class
        {
            return Set<T>();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AccountEntityTypeConfiguration).Assembly);
        }
    }
}