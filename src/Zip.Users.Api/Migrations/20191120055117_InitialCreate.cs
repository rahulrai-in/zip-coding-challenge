﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zip.Users.Api.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                "users");

            migrationBuilder.CreateSequence(
                "account_hilo",
                "users",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                "zipuser_hilo",
                "users",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                "zipuser",
                schema: "users",
                columns: table => new
                {
                    Id = table.Column<int>(),
                    Name = table.Column<string>(nullable: true),
                    Email = table.Column<string>(maxLength: 50),
                    MonthlySalary = table.Column<double>(),
                    MonthlyExpense = table.Column<double>()
                },
                constraints: table => { table.PrimaryKey("PK_zipuser", x => x.Id); });

            migrationBuilder.CreateTable(
                "account",
                schema: "users",
                columns: table => new
                {
                    Id = table.Column<int>(),
                    Name = table.Column<string>(maxLength: 50),
                    UserId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_account", x => x.Id);
                    table.ForeignKey(
                        "FK_account_zipuser_UserId",
                        x => x.UserId,
                        principalSchema: "users",
                        principalTable: "zipuser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                "IX_account_UserId",
                schema: "users",
                table: "account",
                column: "UserId");

            migrationBuilder.CreateIndex(
                "IX_zipuser_Email",
                schema: "users",
                table: "zipuser",
                column: "Email",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "account",
                "users");

            migrationBuilder.DropTable(
                "zipuser",
                "users");

            migrationBuilder.DropSequence(
                "account_hilo",
                "users");

            migrationBuilder.DropSequence(
                "zipuser_hilo",
                "users");
        }
    }
}