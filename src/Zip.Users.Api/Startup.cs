using System;
using FluentValidation;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Zip.Users.Api.Application.Behavior;
using Zip.Users.Api.Application.Queries;
using Zip.Users.Api.Application.Validations;
using Zip.Users.Api.Extensions;
using Zip.Users.Api.Infrastructure;
using Zip.Users.Api.Infrastructure.Repositories;

namespace Zip.Users.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Zip Users Api", Version = "v1" });
            });
            services.AddEntityFrameworkSqlServer().AddDbContext<UsersContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("ZipUsers"),
                    sqlOptions => { sqlOptions.EnableRetryOnFailure(10, TimeSpan.FromSeconds(30), null); });
            });

            // registrations
            services.AddScoped<UsersContext>();
            services.AddScoped<IUsersContext, UsersContext>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<IUserQueries, UserQueries>();
            services.AddScoped<IAccountQueries, AccountQueries>();
            services.AddScoped<IMediator, Mediator>();
            services.AddMediatR(GetType().Assembly);
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(ValidatorBehavior<,>));
            services.AddValidatorsFromAssemblyContaining<CreateUserCommandValidator>();
            services.AddMvc().AddFluentValidation(fv =>
                fv.RegisterValidatorsFromAssemblyContaining<CreateAccountCommandValidator>());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ApplicationExceptionMiddleware>();
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Zip Users Api v1"); });
        }
    }
}