﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Xunit;
using Zip.Users.Api;
using Zip.Users.Api.Application.Commands;
using Zip.Users.Api.Domain.Aggregates;
using Zip.Users.Api.Infrastructure;

namespace Zip.Users.Tests.FunctionalTests
{
    public class UserTests
        : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;

        public UserTests(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            using var serviceScope = _factory.Services.CreateScope();
            var context = serviceScope.ServiceProvider.GetRequiredService<UsersContext>();
            context.Database.EnsureCreated();
        }

        [Fact]
        public async Task AddAndGetUsers()
        {
            // Arrange
            var name = $"Test-{new Random().Next(100, 999)}";
            var client = _factory.CreateClient();
            var content = new StringContent(CreateUserCommand(name), Encoding.UTF8, "application/json");

            // Act
            var postResponse = await client.PostAsync("/api/v1/Users", content);
            postResponse.EnsureSuccessStatusCode();
            var getResponse = await client.GetAsync("/api/v1/Users");
            getResponse.EnsureSuccessStatusCode();

            // Assert
            Assert.Equal(HttpStatusCode.OK, postResponse.StatusCode);
            Assert.Equal(HttpStatusCode.OK, getResponse.StatusCode);
            var contents = await getResponse.Content.ReadAsStringAsync();
            var zipUsers = JsonConvert.DeserializeObject<List<ZipUser>>(contents);
            Assert.Contains(zipUsers, user => user.Name == name);
        }

        private string CreateUserCommand(string name)
        {
            var user = new CreateUserCommand()
            {
                Name = name,
                Email = $"{name}@my-mail.com",
                MonthlyExpense = 10,
                MonthlySalary = 5
            };
            return JsonConvert.SerializeObject(user);
        }
    }
}