﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoNSubstitute;
using FluentAssertions;
using FluentValidation;
using NSubstitute;
using Xunit;
using Zip.Users.Api.Application.CommandHandler;
using Zip.Users.Api.Application.Commands;
using Zip.Users.Api.Domain.Aggregates;
using Zip.Users.Api.Infrastructure.Repositories;

namespace Zip.Users.Tests.UnitTests.CommandHandler
{
    public class CreateUserCommandHandlerTests
    {
        public CreateUserCommandHandlerTests()
        {
            _userRepository = _fixture.Freeze<IUserRepository>();
        }

        private readonly IFixture _fixture =
            new Fixture().Customize(new AutoNSubstituteCustomization { ConfigureMembers = true });

        private readonly IUserRepository _userRepository;

        [Fact]
        public async Task CommandFailsIfUserExists()
        {
            // arrange
            var commandHandler = _fixture.Create<CreateUserCommandHandler>();
            var user = new ZipUser("Test Name", "email@email.com", 10, 11);
            _userRepository.GetUserByEmailAsync(Arg.Is("email@email.com")).Returns(Task.FromResult(user));
            var command = new CreateUserCommand
            {
                Email = user.Email,
                MonthlySalary = user.MonthlySalary,
                MonthlyExpense = user.MonthlyExpense,
                Name = user.Name
            };

            // act
            Func<Task<bool>> action = () => commandHandler.Handle(command, CancellationToken.None);

            // assert
            action.Should().Throw<ValidationException>().Where(ex => ex.Message == "User already exists");
        }

        [Fact]
        public async Task CommandSucceedsForNewUser()
        {
            // arrange
            var commandHandler = _fixture.Create<CreateUserCommandHandler>();
            var user = new ZipUser("Test Name", "email@email.com", 10, 11);
            _userRepository.GetUserByEmailAsync(Arg.Is("email@email.com")).Returns(Task.FromResult(default(ZipUser)));
            _userRepository.SaveChangesAsync().Returns(Task.FromResult(true));
            var command = new CreateUserCommand
            {
                Email = user.Email,
                MonthlySalary = user.MonthlySalary,
                MonthlyExpense = user.MonthlyExpense,
                Name = user.Name
            };

            // act
            var result = await commandHandler.Handle(command, CancellationToken.None);

            // assert
            result.Should().BeTrue();
        }
    }
}