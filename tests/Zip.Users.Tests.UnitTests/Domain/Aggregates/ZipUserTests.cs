using Xunit;
using Zip.Users.Api.Domain.Aggregates;

namespace Zip.Users.Tests.UnitTests.Domain.Aggregates
{
    public class ZipUserTests
    {
        [Fact]
        public void AddAccountAddsNewAccountForUser()
        {
            // arrange
            var user = new ZipUser("testName", "testEmail", 1,1);

            // act
            user.AddAccount("account");

            // assert
            Assert.Contains(user.Accounts, account => account.Name.Equals("account"));
        }
    }
}
